
plugin.tx_hiveextcookie_hiveextcookienoticerender {
    view {
        # cat=plugin.tx_hiveextcookie_hiveextcookienoticerender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_cookie/Resources/Private/Templates/
        # cat=plugin.tx_hiveextcookie_hiveextcookienoticerender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_cookie/Resources/Private/Partials/
        # cat=plugin.tx_hiveextcookie_hiveextcookienoticerender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_cookie/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextcookie_hiveextcookienoticerender//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
plugin {
    tx_hiveextcookie {
        model {
            HIVE\HiveExtCookie\Domain\Model\Notice {
                persistence {
                    storagePid =
                }
            }
        }
    }
}
plugin.tx_hiveextcookie_hiveextcookienoticerender {
    settings {
        render {

        }
    }
}