<?php
namespace HIVE\HiveExtCookie\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_cookie" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Notice
 */
class Notice extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * text
     *
     * @var string
     */
    protected $text = '';

    /**
     * position
     *
     * @var int
     */
    protected $position = 0;

    /**
     * detaillink
     *
     * @var string
     */
    protected $detaillink = '';

    /**
     * lifetime
     *
     * @var int
     */
    protected $lifetime = 0;

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the position
     *
     * @return int $position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the position
     *
     * @param int $position
     * @return void
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Returns the detaillink
     *
     * @return string $detaillink
     */
    public function getDetaillink()
    {
        return $this->detaillink;
    }

    /**
     * Sets the detaillink
     *
     * @param string $detaillink
     * @return void
     */
    public function setDetaillink($detaillink)
    {
        $this->detaillink = $detaillink;
    }

    /**
     * Returns the lifetime
     *
     * @return int $lifetime
     */
    public function getLifetime()
    {
        return $this->lifetime;
    }

    /**
     * Sets the lifetime
     *
     * @param int $lifetime
     * @return void
     */
    public function setLifetime($lifetime)
    {
        $this->lifetime = $lifetime;
    }
}
