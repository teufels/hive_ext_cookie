<?php
namespace HIVE\HiveExtCookie\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class NoticeTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtCookie\Domain\Model\Notice
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtCookie\Domain\Model\Notice();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText()
        );
    }

    /**
     * @test
     */
    public function setTextForStringSetsText()
    {
        $this->subject->setText('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPositionReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getPosition()
        );
    }

    /**
     * @test
     */
    public function setPositionForIntSetsPosition()
    {
        $this->subject->setPosition(12);

        self::assertAttributeEquals(
            12,
            'position',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDetaillinkReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getDetaillink()
        );
    }

    /**
     * @test
     */
    public function setDetaillinkForStringSetsDetaillink()
    {
        $this->subject->setDetaillink('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'detaillink',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLifetimeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getLifetime()
        );
    }

    /**
     * @test
     */
    public function setLifetimeForIntSetsLifetime()
    {
        $this->subject->setLifetime(12);

        self::assertAttributeEquals(
            12,
            'lifetime',
            $this->subject
        );
    }
}
